angular.module('SelecaoPlano')
    .controller('PrincipalController', ['$scope', "PlataformaService", "PlanosService", "$uibModal", PrincipalController]);


function PrincipalController($scope, PlataformaService, PlanosService, $uibModal) {
    var vm = this;

    vm.selecaoPlanoCliente = {
        plataformaSku: '',
        plataforma: '',
        plano: '',
    };

    vm.openModal = (skuPlano) => {

        vm.selecaoPlanoCliente.plano = vm.planosAtivos.filter(function (plano) {
            return plano.sku == skuPlano;
        })[0];

        $uibModal.open({
            templateUrl: 'app/modal/modal.html',
            controller: 'ModalController',
            controllerAs: 'principal',
            resolve: {
                selecaoPlanoCliente: function () {
                    return vm.selecaoPlanoCliente;
                }
            }
        });
    };

    var GetPlataformas = () =>
        PlataformaService.Get()
            .then(plataformas => {
                vm.plataformas = plataformas.data;
                vm.plataformas = vm.plataformas.plataformas;


                for (x = 0; x < vm.plataformas.length; x++) {
                    //Adicionar Icones
                    if (vm.plataformas[x].sku == 'TBT01') {
                        vm.plataformas[x].icone = 'tablet';
                    }
                    if (vm.plataformas[x].sku == 'CPT02') {
                        vm.plataformas[x].icone = 'desktop';
                    }
                    if (vm.plataformas[x].sku == 'WF03') {
                        vm.plataformas[x].icone = 'wifi';
                    }

                    //Remover o "|" da descrição das plataformas
                    vm.plataformas[x].descricao = vm.plataformas[x].descricao.replace(/\|/g, " ");
                }

            });
    GetPlataformas();

    vm.SelecionaPlano = (skuPlataforma) =>
        PlanosService.Get(skuPlataforma)
            .then(planoSelecionado => {

                vm.planosAtivos = planoSelecionado.data.planos.filter(function (plano) {
                    return plano.ativo == true;
                });

                if (skuPlataforma == 'TBT01') {
                    vm.plataformaSelecionada = 'tablet';
                }
                if (skuPlataforma == 'CPT02') {
                    vm.plataformaSelecionada = 'computador';
                }
                if (skuPlataforma == 'WF03') {
                    vm.plataformaSelecionada = 'wi-fi';
                }


                vm.selecaoPlanoCliente.plataformaSku = skuPlataforma;
                vm.selecaoPlanoCliente.plataforma = vm.plataformaSelecionada;


                vm.cols = 12 / vm.planosAtivos.length;
            });

}