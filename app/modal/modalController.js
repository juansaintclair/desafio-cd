angular.module('SelecaoPlano')
    .controller('ModalController', ['selecaoPlanoCliente', '$uibModalInstance', ModalController]);

function ModalController(selecaoPlanoCliente, $uibModalInstance) {
  var vm = this;
  
  vm.selecaoPlanoCliente = selecaoPlanoCliente;

  vm.enviarDados = () => console.log(vm.selecaoPlanoCliente);

  vm.fechaModal = () => $uibModalInstance.dismiss('cancel');
}