/* global angular */

angular.module("SelecaoPlano")
    .directive("loading", [loading]);

function loading() {
    return {
        templateUrl: "/app/loading/loading.html",
        restrict: "E"
    }
}