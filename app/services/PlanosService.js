/* global angular */

angular.module("SelecaoPlano")
    .service("PlanosService", ["$http", "singleton", PlanosService]);

function PlanosService($http, singleton) {

    return {
        Get: skuPlataforma => $http.get(singleton.api + "planos/" + skuPlataforma)
    }
};