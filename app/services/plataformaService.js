/* global angular */

angular.module("SelecaoPlano")
    .service("PlataformaService", ["$http", "singleton", PlataformaService]);

function PlataformaService($http, singleton) {

    return {
        Get: () => $http.get(singleton.api + "plataformas")
    }
};