angular.module("SelecaoPlano", ['ui.bootstrap', 'ui.mask', 'ngMessages'])
    .config(function ($httpProvider) {
        $httpProvider.defaults.useXDomain = true;

        delete $httpProvider.defaults.headers.common["X-Requested-With"];
        $httpProvider.interceptors.push("interceptor");
    });
