/* global angular */

var singleton = {
    api: "http://private-59658d-celulardireto2017.apiary-mock.com/"
};

angular.module("SelecaoPlano")
    .constant("singleton", singleton);